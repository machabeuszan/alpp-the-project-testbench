/*---------------------------------------------+
|                                              |
| Name: hwtest/main.cpp                        |
|  Project: alpp                               |
|  Author: Adrian Ciesielski                   |
|  Creation Date: 2012-10-06                   |
|  Copyright: (c) 2012 by Adrian Ciesielski    |
|  License: See top of Makefile                |
|  Version: See top of Makefile                |
|                                              |
+---------------------------------------------*/

#include <avr/io.h>
#include <util/delay.h>
#include "sys/pled.h"
#include "hal/pgpipin.h"
#include "hal/pusartcommon.h"

#define megax88_UART_REGS  UCSR0A, UCSR0B, UCSR0C, UDR0, UBRR0L, UBRR0H


#if 0
typedef volatile uint8_t REG8;

class Uuu
{
    public:
        Uuu(REG8 &ucsra,REG8 &ucsrb, REG8 &ucsrc, REG8 &udr, REG8 &ubrrl, REG8 &ubrrh);
        void echoplus(void);

    private:
       volatile uint8_t &ucsra;
       volatile uint8_t &ucsrb;
       volatile uint8_t &ucsrc;
       volatile uint8_t &udr;
       volatile uint8_t &ubrrl;
       volatile uint8_t &ubrrh;
};

Uuu::Uuu(REG8 &UCSRA,REG8 &UCSRB, REG8 &UCSRC, REG8 &UDR, REG8 &UBRRL, REG8 &UBRRH):
    ucsra(UCSRA),
    ucsrb(UCSRB),
    ucsrc(UCSRC),
    udr(UDR),
    ubrrl(UBRRL),
    ubrrh(UBRRH)
{
    ucsra = 0;
    ucsrb = _BV(RXEN0) | _BV(TXEN0);
    ucsrc = _BV(UCSZ01) | _BV(UCSZ00);

    const uint8_t ubrr = 12; // 38,4kBaud @ 8MHz
    ubrrh = ubrr>>8;
    ubrrl = ubrr;
}

void Uuu::echoplus()
{
    unsigned char znak;
    if( ucsra & _BV(RXC0) )
    {
        znak = udr+1;
        while( !(ucsra & (1<<UDRE0)));
        UDR0 = znak;
    }
}
#endif

int main(void)
{

    PLed led1(PORTD,7,LO,250);
    PLed led2(PORTB,0,LO,250);
    PGpiPin sw1(PORTD,6,HI);
    PGpiPin sw2(PORTD,5,HI);
    led1.setPercent(5);


    PUsartCommon serial(megax88_UART_REGS);
    serial.config(PUsartCommon::P_BAUD_38400);


    //Uuu uart(megax88_UART_REGS);
    unsigned char znak = 'a';

    while(1)
    {
		_delay_us(40);
		led1.poll();

        //serial.txChar(znak);

        if(serial.rxReady())
        {
            znak = serial.rxChar();
            serial.txChar(znak+1);
        }
        //uart.echoplus();

        #if 0
        if(znak < 'z')
        {
            znak++;
        } else {
            znak='a';
        }
        #endif

		if(sw1.isOn())
		{
			led1.setPercent(70);
		} else {
			led1.setPercent(20);
		}

		if(sw2.isOn())
		{
            led2.on();
		} else {
            led2.off();
		}


    }

    return 0;
}
